/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2014  David Edmundson <davidedmundson@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "reditem.h"

#include <QSGMaterialShader>
#include <QSGMaterial>
#include <QSGGeometryNode>

class RedMaterialShader : public QSGMaterialShader
{
public:
    virtual const char* const* attributeNames() const
    {
        static char const *const attr[] = { "vCoord", 0 };
        return attr;
    }

    virtual const char* vertexShader() const
    {
        return  "attribute highp vec4 vCoord;"
                "uniform highp mat4 matrix;"
                "void main()"
                "{"
                    "gl_Position = matrix * vCoord;"
                "}";
    }

    virtual const char* fragmentShader() const
    {
        //fragment shaders adjust the colour of each pixel
        //there is a special variable gl_FragColor which determines the colour of
        //the pixel at coord
        //in our case, always red
        return "void main() {"
               "  gl_FragColor = vec4(1, 0, 0, 1);" //R, G, B, A
               "}";
    }
    virtual void updateState(const RenderState& state, QSGMaterial* newMaterial, QSGMaterial* oldMaterial)
    {
        if (state.isMatrixDirty()) {
            program()->setUniformValue("matrix", state.combinedMatrix());
            //in reality you want to cache the uniform location to save a lookup on every update
        }
    }

    static QSGMaterialType type;
};

QSGMaterialType RedMaterialShader::type;

class RedMaterial : public QSGMaterial
{
public:
    virtual QSGMaterialShader* createShader() const {
        return new RedMaterialShader;
    }
    virtual QSGMaterialType* type() const {
             return &RedMaterialShader::type;
    }
};

RedItem::RedItem(QQuickItem* parent):
    QQuickItem(parent)
{
    setFlag(QQuickItem::ItemHasContents, true);
}

RedItem::~RedItem()
{
}

QSGNode* RedItem::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData*)
{
    QSGGeometryNode *node = static_cast<QSGGeometryNode*>(oldNode);

    if (!node) {
        node = new QSGGeometryNode;

        RedMaterial *m = new RedMaterial;
        node->setMaterial(m);
        node->setFlag(QSGNode::OwnsMaterial, true);

        QSGGeometry *g = new QSGGeometry(QSGGeometry::defaultAttributes_TexturedPoint2D(), 4);
        node->setGeometry(g);
        node->setFlag(QSGNode::OwnsGeometry, true);
    }

    QSGGeometry::updateTexturedRectGeometry(node->geometry(), QRect(0,0, width(), height()), QRect(0,0,1,1));

    return node;
}


#include "moc_reditem.cpp"
