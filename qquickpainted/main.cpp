#include <QGuiApplication>
#include <QQuickView>

#include "reditem.h"
#include <QtQml/QQmlContext>

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

    qmlRegisterType<RedItem>("org.dave", 1, 0, "RedItem");

    QQuickView view;
    view.resize(800, 400);
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl("main.qml"));
    view.show();

    return a.exec();
}
