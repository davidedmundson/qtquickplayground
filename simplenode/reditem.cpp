/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2014  David Edmundson <davidedmundson@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "reditem.h"

#include <QSGSimpleRectNode>


RedItem::RedItem(QQuickItem* parent): QQuickItem(parent)
{
    setFlag(QQuickItem::ItemHasContents, true);
}

RedItem::~RedItem()
{

}


QSGNode* RedItem::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData*)
{
    QSGSimpleRectNode *rectNode = static_cast<QSGSimpleRectNode*>(oldNode);
    if (!rectNode) {
        rectNode = new QSGSimpleRectNode;
        rectNode->setColor(QColor(Qt::red));
    }
    rectNode->setRect(0, 0, width(), height());
    return rectNode;
}


#include "moc_reditem.cpp"
